#!/bin/sh
#
# The script requires AWS CLI tools to be installed
#

S3_BASE_URL=s3://net.hetras-staging.backups/jenkins/
JENKINS_HOME=/var/jenkins
BACKUP_FILE="jenkins-`date +%Y-%m-%d`.tar.bz2"

tar --exclude=logs --exclude=war --exclude=workspace \
    -cPf - $JENKINS_HOME | bzip2 -9c > $BACKUP_FILE

if [ $? -eq 0 ]; then
    aws s3 cp $BACKUP_FILE $S3_BASE_URL --expires `date --date="next day" +%F` 2>&1 > /dev/null
    rm -f $BACKUP_FILE
fi
