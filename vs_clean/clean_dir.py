#!/usr/bin/env python

import sys
from os import listdir
from os.path import isdir, join
from shutil import rmtree

def do_cleanup(dir):
    content = listdir(dir)
    if 'obj' in content and 'bin' in content:
        print 'Cleaning up a project dir found %s' % dir
        rmtree(join(dir, 'obj'))
        rmtree(join(dir, 'bin'))
        return
    for sub_dir in ((join(dir, s) for s in content)):
        if isdir(sub_dir):
            do_cleanup(sub_dir)

root = sys.argv[1] if len(sys.argv) > 1 else '.'

do_cleanup(root)