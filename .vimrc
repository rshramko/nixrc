execute pathogen#infect()
set nocompatible
filetype indent plugin on

syntax on

set wildmenu
set showcmd
set hlsearch

set nocp
" When 'ignorecase' and 'smartcase' are both on, if a pattern contains an uppercase letter, 
" it is case sensitive, otherwise, it is not
set smartcase
set ignorecase

set ruler
set nostartofline
set laststatus=2
set confirm
set hidden

set mouse=a
set pastetoggle=<F11>

set autoindent
set autowrite
set number
set shiftwidth=2
set softtabstop=2
set expandtab
set smarttab
set viminfo='100,f1

